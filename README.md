# TDAmeritrade Trader

## Code's walkthrough
The trader script uses the config.ini file. There you can adjust the time for entering and exiting the market, the margin of capital that won't be used for trading and the instruments to trade with.

The refresh_token calls are set to be run just before expiring times.

When entering in a position, the code uses a JSON template and just add the instrument's symbol and the quantity for each instrument which is set to US dollars, hence there's no need to calculate the number of shares, because the capital used is gathered from the API. Later, on exit time, the position is closed by passing the last order Id *(the same order id created on enter)* and the symbol.

## Configuring the time.
The script parses the time configuration using the syntax of a crontab.

Example1:
entry_time=10:00:00
exit_time=15:59:45
The trader enters a position at 10am and exits 15 seconds before the market closes.

Example2:
entry_time=*:00:00
exit_time=*:50:00
The trader enters a position every hour and exits 50 minutes later.

## How to run the script
1. First fill up the config.ini file and introduce the desired parameters.

### Sample Config: config.ini
```
# Configuration file for trader.

[authentication]

API_KEY=1931632518@AMER.OAUTHAP
REDIRECT_URI=http://localhost
account_id=754309205
REDIRECT_SERVER_PORT=443

[user variables]
#instruments
symbol1=QQQ
symbol2=SPY

# Minimum reserved capital
margin=10000

# Entry market time
# valid format
# hour:minute:second
entry_time=*:*:10

# Exit market time
# valid format
# hour:minute:second
exit_time=*:*:40

```

2. Run the trader with the following command at the command line interface
```bash

# Linux, run with sudo to let the local server be binded
path/to/td-ameritrade-trader$ sudo python3 trader.py 

# Windows, allow the firewall to use the server ports
C:\path\to\project_folder> python trader.py

# Windows alternative
double-click the run_trader.bat to run it without manually opening a CMD window.

```

3. The script will need you to login using the browser. Then Log in and TDAmeritrade and when getting the "Logged!" page just watch the command line prompt

 <h3>If an error shows up for invalid cert. Please ignore it and try to get in by selecting advanced configuration (Chrome)</h3>




